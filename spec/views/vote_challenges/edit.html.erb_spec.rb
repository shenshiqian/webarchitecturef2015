require 'rails_helper'

RSpec.describe "vote_challenges/edit", type: :view do
  before(:each) do
    @vote_challenge = assign(:vote_challenge, VoteChallenge.create!(
      :user_id => 1,
      :challenge_id => 1
    ))
  end

  it "renders the edit vote_challenge form" do
    render

    assert_select "form[action=?][method=?]", vote_challenge_path(@vote_challenge), "post" do

      assert_select "input#vote_challenge_user_id[name=?]", "vote_challenge[user_id]"

      assert_select "input#vote_challenge_challenge_id[name=?]", "vote_challenge[challenge_id]"
    end
  end
end
