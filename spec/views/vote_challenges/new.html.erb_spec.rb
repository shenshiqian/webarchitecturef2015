require 'rails_helper'

RSpec.describe "vote_challenges/new", type: :view do
  before(:each) do
    assign(:vote_challenge, VoteChallenge.new(
      :user_id => 1,
      :challenge_id => 1
    ))
  end

  it "renders new vote_challenge form" do
    render

    assert_select "form[action=?][method=?]", vote_challenges_path, "post" do

      assert_select "input#vote_challenge_user_id[name=?]", "vote_challenge[user_id]"

      assert_select "input#vote_challenge_challenge_id[name=?]", "vote_challenge[challenge_id]"
    end
  end
end
