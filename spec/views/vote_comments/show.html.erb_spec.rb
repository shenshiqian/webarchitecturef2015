require 'rails_helper'

RSpec.describe "vote_comments/show", type: :view do
  before(:each) do
    @vote_comment = assign(:vote_comment, VoteComment.create!(
      :user_id => 1,
      :comment_id => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
  end
end
