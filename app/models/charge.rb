class Charge < ActiveRecord::Base

  has_many :users
  has_many :challenges

  validates_presence_of :amount
  validates_presence_of :challenge_id
  validates_presence_of :description
  validates_presence_of :user_id


end
