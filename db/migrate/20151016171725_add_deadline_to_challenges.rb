class AddDeadlineToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :deadline, :date
  end
end
